var mongoose    = require("mongoose"),
Camp            = require("./models/campground"),
Comment         = require("./models/comment");

var data = [
    {
        name: "Uno",
        image: "http://lorempixel.com/output/nature-q-c-400-300-7.jpg",
        place: "Palma",
        rating: 4,
        description: "Es un sitio espectacular!",
        author: {
            username: "Carlos",
            id: "59dddc07e551b4093c9d7bf4"
        }
    },
    {
        name: "Dos",
        image: "http://lorempixel.com/output/animals-q-c-640-480-9.jpg",
        place: "Playa de Muro",
        rating: 4,
        description: "Aceptan Mascotas!",
        author: {
            username: "Luis",
            id: "59e4b5431537df143cb55902"
        }
    },
    {
        name: "Tres",
        image: "http://lorempixel.com/output/nature-q-c-640-480-5.jpg",
        place: "Alcudia",
        rating: 4,
        description: "Para despertar con el sonido del mar",
        author: {
            username: "Laura",
            id: "59e4af11275c21140b8a14c2"
        }
    },
    {
        name: "Cuatro",
        image: "http://lorempixel.com/output/nature-q-c-640-480-8.jpg",
        place: "Tramuntana",
        rating: 4,
        description: "Para quienes disfrutan más de la montaña",
        author: {
            username: "Roger",
            id: "59e4b5871537df143cb55903"
        }
    },
     {
        name: "Cinco",
        image: "http://lorempixel.com/output/nature-q-c-640-480-2.jpg",
        place: "Tramuntana",
        rating: 4,
        description: "Para quienes disfrutan más de la montaña",
         author: {
            username: "Carlos",
            id: "59dddc07e551b4093c9d7bf4"
        }
    },
     {
        name: "Seis",
        image: "http://lorempixel.com/output/nature-q-c-640-480-3.jpg",
        place: "Tramuntana",
        rating: 4,
        description: "Para quienes disfrutan más de la montaña",
        author: {
            username: "Luis",
            id: "59e4b5431537df143cb55902"
        }
    },
     {
        name: "Siete",
        image: "http://lorempixel.com/output/nature-q-c-640-480-4.jpg",
        place: "Tramuntana",
        rating: 4,
        description: "Para quienes disfrutan más de la montaña",
         author: {
            username: "Laura",
            id: "59e4af11275c21140b8a14c2"
        }
    },
     {
        name: "Ocho",
        image: "http://lorempixel.com/output/nature-q-c-640-480-6.jpg",
        place: "Tramuntana",
        rating: 4,
        description: "Para quienes disfrutan más de la montaña" ,
        author: {
            username: "Tomeu",
            id: "59e4b5871537df143cb55903"
        }
    }
    
    ];


function seedDB(){
    //remove campgrounds
    Camp.remove({}, function(err){
    if(err){
        console.log(err);
    }
    console.log("removed campgrounds");
    //add campgrounds
    data.forEach(function (seed){
        Camp.create(seed, function(err, campground){
            if(err){
                console.log(err);
            } else {
                console.log("Added a campground");   
                //add comments
                Comment.create(
                    { 
                        text : "I like this place but I wish there was internet",
                        author: "Carlos"
                        
                    }, function (err, comment){
                        if(err){
                            console.log()
                        } else {
                            comment.author.username = "Carlos";
                            comment.save();
                            campground.comments.push(comment);
                            campground.save();
                            console.log("Create a new comment");
                        }
                    }
                    )
            }
        } );
    });
 
});
}

module.exports = seedDB;