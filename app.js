var express             = require ("express"),
bodyParser              = require("body-parser"),
mongoose                = require("mongoose"),
passport                = require("passport"),
LocalStrategy           = require ("passport-local"),
methodOverride         = require("method-override"),
passportLocalMongoose   = require ("passport-local-mongoose"),
Camp                    = require("./models/campground"),
Comment                 = require("./models/comment"),
User                    = require("./models/user"),
flash    = require ("connect-flash"),

//seedDB      = require("./seed"),
app                     = express();
//seedDB();


app.use(flash());

var indexRoutes = require ("./routes/index");
var commentRoutes = require ("./routes/comments");
var campgroundRoutes = require ("./routes/campgrounds");


mongoose.connect("mongodb://localhost/yelp_app_v3", {useMongoClient: true});
//mongoose.connect("mongodb://cmarchena:gm1929hw@ds121575.mlab.com:21575/yelpapp", {useMongoClient: true});


mongoose.Promise = global.Promise;
app.use(require("express-session")({
    secret: "Madrid es la mejor",
    resave: false,
    saveUninitialized: false,
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(express.static("public"));
app.set("view engine", "ejs");
passport.use(new LocalStrategy(User.authenticate()));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
app.use(function (req, res, next){
    res.locals.currentUser =req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
    });
app.use(methodOverride("_method"));


app.use("/", indexRoutes);
app.use("/campgrounds/:id/comments", commentRoutes);
app.use("/campgrounds/", campgroundRoutes);
//=================================================================
// Start Server
//=================================================================

 
app.listen(process.env.PORT, process.env.IP, function (){
    console.log("YelpCamp App is running!!!");
})