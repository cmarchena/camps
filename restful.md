RESTFUL ROUTES

name        url         verb        description  
===============================================================================
INDEX       /dogs       GET         Display a list of all dogs
NEW         /dogs/new   GET         Display form to make a new dogs
CREATE      /dogs       POST        Adds a new dog to the db
SHOW        /dogs/:id   GET         Shows info about one dog
