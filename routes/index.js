var express = require ("express"),
passport                = require("passport"),
LocalStrategy           = require ("passport-local"),
passportLocalMongoose   = require ("passport-local-mongoose");
var router = express.Router();
var User                    = require("../models/user");



passport.use(new LocalStrategy(User.authenticate()));
router.use(passport.initialize());
router.use(passport.session());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

router.get("/", function(req, res){
    res.render("campgrounds/landing");
    //res.redirect("/campgrounds");
});

//====================================================================
// Auth Routes
//====================================================================

router.get("/user-area", isLoggedIn, function(req, res){
    User.find({}, function(err, users){
    if(err){
        console.log("There is an error in finding all users");
    } else{
        res.render("user-area/dashboard", { users: users, currentUser: req.user});
       console.log("All the users");
        console.log(users);
    }
})
});




//REGISTER

router.get("/register", function(req, res){
    res.render("user-area/new-user");
   
});

router.post("/register", function(req, res){
    User.register(new User({username: req.body.username}), req.body.password, function(err,user){
        if(err){
            console.log(err)
            req.flash("error", err.message)
            return res.redirect("/register")
            }
            passport.authenticate("local")(req,res, function (){
                req.flash("success", "Bienvenido " + user.username)
                res.redirect("/user-area")
                console.log(user)
            })
    })
   
   
});

//LOGIN
router.get("/login", function(req, res){
    res.render("user-area/login");
   
});

router.post("/login", passport.authenticate("local",{
    successRedirect: "/user-area",
    failureRedirect: "login"
}), function (res, req){
    }
);
//LOGOUT
router.get("/logout", function(req,res){
    req.logout()
    req.flash("success", "Has cerrado sesión, ¡Hasta luego!")
    res.redirect("/campgrounds")
})

//MIDDLEWARE

function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect("/login")
}
module.exports = router;