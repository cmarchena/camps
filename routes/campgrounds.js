var express = require ("express");
var router = express.Router();
var Camp  = require("../models/campground");
var middleware = require ("../middleware")


//=================================================
// CAMPGROUNDS
//=================================================
//INDEX ROUTE - Display a list of all camps
router.get("/", function(req, res){
    Camp.find({}, function(err, camps){
    if(err){
        console.log("There is an error in finding all camps");
    } else{
        res.render("campgrounds/index", { camps: camps, currentUser: req.user});
        console.log("All the camps");
        console.log(camps);
    }
});    
    
   
});
//NEW ROUTE - Diplay form to add new camp
router.get("/new", middleware.isLoggedIn, function(req, res){
    res.render("campgrounds/new");
});

//CREATE ROUTE - Add new camp to the database
router.post("/", middleware.isLoggedIn, function(req, res){
    var name = req.body.name;
    var image = req.body.image;
    var place = req.body.place;
    var rating = req.body.rating;
    var description = req.body.description;
    var author = {
        username : req.user.username,
        id : req.user._id
    }
    var newCamp = {
    name: name,
    image: image,
    place: place,
    rating: rating,
    description: description,
    author: author
};
    Camp.create(newCamp, function (err, camp){
    if(err){
        console.log("Something went wrong!");
    }else{
        req.flash("success", "¡Tu campamento ha sido creado!")
        res.redirect("/campgrounds");
        console.log("We just saved a camp to the db!");
        console.log(camp);
    }
});
   
});
//SHOW ROUTE - Shows info about one camp 

router.get("/:id", function(req, res){
    
    Camp.findById(req.params.id).populate("comments").exec(function(err, foundCamp){
        if(err){
         console.log("There is an error in finding the camps");
     } else{
         res.render("campgrounds/camp", { camp: foundCamp});
        
     } });
});



//EDIT ROUTE
router.get("/:id/edit", middleware.checkCampOwnership, function(req, res) {
    Camp.findById(req.params.id, function (err, foundCamp){
        if(err){
            console.log(err)
        } else {
            res.render("campgrounds/edit", {camp: foundCamp})
        }
    })
})
  
        
    
    


//UPDATE ROUTE
router.put("/:id", middleware.checkCampOwnership, function (req, res){
    
    Camp.findByIdAndUpdate(req.params.id, req.body.editedCamp, function (err, editedCamp){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Campamento Actualizado")
            res.redirect("/campgrounds/" + req.params.id)
        }
    } )
})


//DESTROY ROUTE
router.delete("/:id", middleware.checkCampOwnership, function (req, res){
       Camp.findByIdAndRemove(req.params.id, function (err){
        if(err){
            console.log(err)
        } else {
            console.log("Campamento Eliminado")
            req.flash("success", "Campamento Eliminado")
            res.redirect("/campgrounds" )
        }
    } )
})







module.exports = router;