var express = require ("express");
var router = express.Router({mergeParams: true});
var Camp  = require("../models/campground");
var Comment                 = require("../models/comment");
var middleware = require ("../middleware/index.js")

// =========================================================
// COMMENTS ROUTES
// =========================================================

// NEW
router.get("/new", middleware.isLoggedIn, function(req, res) {
   Camp.findById(req.params.id, function(err, foundCamp){
        if(err){
         console.log(err);
     } else{
         res.render("comments/new", { camp: foundCamp});
        
     } });
});
// CREATE
router.post("/", middleware.isLoggedIn, function(req, res){
    Camp.findById(req.params.id, function(err, campground) {
        if(err){
            console.log(err);
        } else{
            Comment.create(req.body.comment, function(err,comment){
                if(err){
                    console.log(err);
                } else {
                    comment.author.username = req.user.username;
                    comment.author.id = req.user._id;
                    comment.save();
                    campground.comments.push(comment);
                    campground.save();
                    req.flash("success", "¡Tu comentario ha sido creado!")
                    res.redirect("/campgrounds/" + campground._id );
                }
            });
        }
    });
  
});
//EDIT
router.get("/:comment_id/edit", middleware.checkCommentOwnership, function (req, res){
    Comment.findById(req.params.comment_id, function (err, foundComment){
        if(err){
            console.log(err)
        } else {
            res.render("comments/edit", {camp_id: req.params.id, comment: foundComment})
        }
    })
  
})
//UPDATE
router.put("/:comment_id", middleware.checkCommentOwnership, function (req, res){
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function (err, updatedComment){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Tu comentario ha sido editado")
            res.redirect("/campgrounds/" + req.params.id)
        }
    })
    
})

//DESTROY
router.delete("/:comment_id", middleware.checkCommentOwnership, function (req, res){
    Comment.findByIdAndRemove(req.params.comment_id, function (err){
        if(err){
            console.log(err)
        } else {
            req.flash("success", "Comentario eliminado")
            res.redirect("/campgrounds/" + req.params.id)
        }
    })
}
)


module.exports = router;