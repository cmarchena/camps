var Camp  = require("../models/campground"),
Comment = require("../models/comment");
var middlewareObjects = {};
middlewareObjects.checkCommentOwnership = function  (req,res,next){
    if(req.isAuthenticated()){
        Comment.findById(req.params.comment_id, function (err, foundComment){
        if(err){
            res.redirect("back");
        } else {
            if(foundComment.author.id.equals(req.user._id)){
                next();    
            }
            else{
                res.redirect("back");
            }
        }
    });
    } else {
        req.flash("error", "No has creado este campamento, por tanto no puedes editarlo ni borrarlo")
        res.redirect("back");
    }
    }

middlewareObjects.checkCampOwnership = function (req,res,next){
    if(req.isAuthenticated()){
        Camp.findById(req.params.id, function (err, foundCamp){
        if(err){
            res.redirect("back");
        } else {
            if(foundCamp.author.id.equals(req.user._id)){
                next();    
            }
            else{
                res.redirect("back");
            }
        }
    });
    } else {
        req.flash("error", "No has creado este campamento, por tanto no puedes editarlo ni borrarlo")
        res.redirect("back");
    }
    }
middlewareObjects.isLoggedIn = function (req, res, next){
    if(req.isAuthenticated()){
        return next()
    }
    req.flash("error", "Primero necesitas logarte para realizar esta acción")
    res.redirect("/login")
}
module.exports = middlewareObjects