var mongoose = require("mongoose");

var campSchema = new mongoose.Schema({
    name: String,
    image: String,
    place: String,
    rating: Number,
    author: {
        id: {type: mongoose.Schema.Types.ObjectId,
            ref: "User"
    },
        username: String
 },
    description: String,
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Comment"
        }
        ]
});

module.exports = mongoose.model("Camp", campSchema);